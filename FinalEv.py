class Pet():
    def __init__(self,species,name=""):
      self.species=species
      self.name=name
      if self.species != "Dog" and self.species != "Cat" and self.species != "Horse" and self.species != "Hamster":
        raise ValueError("Incorrect species")

    def __str__(self):
        if self.name != "":
            return "Species of: {}, named {}".format(self.species,self.name)
        else:
            return "Species of: {}, unnamed".format(self.species)


class Dog(Pet):
    chases="Cats"
    def __str__(self):
        if self.name != "":
            return "Species of: {}, named {}, chases {}".format(self.species,self.name,self.chases)
        else:
            return "Species of: {}, unnamed, chases {}".format(self.species,self.chases)


class Cat(Pet):
    hates="Dogs"
    def __str__(self):
        if self.name != "":
            return "Species of: {}, named {}, hates {}".format(self.species,self.name,self.hates)
        else:
            return "Species of: {}, unnamed, hates {}".format(self.species,self.hates)

y="y"
while(y=="y"):
    sp=input("Enter Pet Species: ")
    na=input("Enter Pet Name: ")
    if sp=="Dog":
        dog=Dog(sp,na)
        print(dog)
    elif sp=="Cat":
        cat=Cat(sp,na)
        print(cat)
    else:
        pet=Pet(sp,na)
        print(pet)
    y=input("Do you want to continue(y/n)?: ")

